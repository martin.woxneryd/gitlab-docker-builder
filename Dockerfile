FROM java:8

ENV SB_VERSION 1.4.1.RELEASE
ENV SB_PATH http://repo.spring.io/release/org/springframework/boot/spring-boot-cli/${SB_VERSION}
ENV SB_FILE spring-boot-cli-${SB_VERSION}-bin.tar.gz

RUN wget -q ${SB_PATH}/${SB_FILE} && \
	tar -xzf ${SB_FILE} && \
    rm ${SB_FILE} && \
 	mv spring-${SB_VERSION} /opt

ENV PATH=$PATH:/opt/spring-${SB_VERSION}/bin

WORKDIR /src